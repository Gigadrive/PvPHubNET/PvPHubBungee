package net.pvp_hub.bungee.listener;

import net.md_5.bungee.BungeeCord;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.event.LoginEvent;
import net.md_5.bungee.api.event.PostLoginEvent;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.event.EventHandler;
import net.pvp_hub.bungee.PvPHubBungee;
import net.pvp_hub.bungee.api.PlayerUtilities;
import net.pvp_hub.bungee.api.PluginMeta;

public class PlayerJoinListener implements Listener {

	@EventHandler
	public void onPostLoginEvent(PostLoginEvent e) {
		
		try {
			PlayerUtilities.updateOnlineStatus(e.getPlayer(), 1);
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		  
	  	if(e.getPlayer().hasPermission("staff.join")){
	  		int i = 0;
	  		for (ProxiedPlayer p : BungeeCord.getInstance().getPlayers()) {
	  			if(p.hasPermission("staff.join")){
	  				i++;
	  			}
	  		}
	  		e.getPlayer().sendMessage(new TextComponent(PluginMeta.sn_prefix + "Es sind derzeit �a" + i + "�7 Team-Mitglieder online."));
	  		PvPHubBungee.staff.add(e.getPlayer());
	  	}
	  
	    if (e.getPlayer().hasPermission("pvphub.rank.build")) {
		      for (ProxiedPlayer p : BungeeCord.getInstance().getPlayers()) {
		        if (p.hasPermission("staff.join")) {
		          p.sendMessage(new TextComponent(PluginMeta.sn_prefix + "�3" + e.getPlayer().getName() + " �7ist jetzt �aonline"));
		        }
		      }
		    }
	    
	    if (e.getPlayer().hasPermission("pvphub.rank.team")) {
		      for (ProxiedPlayer p : BungeeCord.getInstance().getPlayers()) {
		        if (p.hasPermission("staff.join")) {
		          p.sendMessage(new TextComponent(PluginMeta.sn_prefix + "�a" + e.getPlayer().getName() + " �7ist jetzt �aonline"));
		        }
		      }
		    }
	    
	    if (e.getPlayer().hasPermission("pvphub.rank.mod")) {
		      for (ProxiedPlayer p : BungeeCord.getInstance().getPlayers()) {
		        if (p.hasPermission("staff.join")) {
		          p.sendMessage(new TextComponent(PluginMeta.sn_prefix + "�e" + e.getPlayer().getName() + " �7ist jetzt �aonline"));
		        }
		      }
		    }
	    
	    if (e.getPlayer().hasPermission("pvphub.rank.dev")) {
		      for (ProxiedPlayer p : BungeeCord.getInstance().getPlayers()) {
		        if (p.hasPermission("staff.join")) {
		          p.sendMessage(new TextComponent(PluginMeta.sn_prefix + "�9" + e.getPlayer().getName() + " �7ist jetzt �aonline"));
		        }
		      }
		    }
	    
	    if (e.getPlayer().hasPermission("pvphub.rank.admin")) {
		      for (ProxiedPlayer p : BungeeCord.getInstance().getPlayers()) {
		        if (p.hasPermission("staff.join")) {
		          p.sendMessage(new TextComponent(PluginMeta.sn_prefix + "�c" + e.getPlayer().getName() + " �7ist jetzt �aonline"));
		        }
		      }
		    }
  }
	
	@EventHandler
	public void onCancelJoin(LoginEvent e){
		
		ProxiedPlayer p = BungeeCord.getInstance().getPlayer(e.getConnection().getName());
		System.out.println(e.getConnection().getName());
		
		try {
			if(!PlayerUtilities.isWhitelisted(e.getConnection().getName())){
				e.setCancelled(true);
				e.setCancelReason("�cDu bist nicht gewhitelistet!\nDu ben�tigst einen Beta-Key f�r PvP-Hub.\nDiesen Beta Key kannst du von den Admins bekommen.\nL�se diesen Key dann auf\n�ehttp://www.pvp-hub.net/redeem �cein.");
			}
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		if((PvPHubBungee.isInMaintenance) && (!p.hasPermission("pvphub.joinMaintenance"))){
			e.setCancelled(true);
			e.setCancelReason(PluginMeta.prefix + "�cDer Server ist im Wartungsmodus.");
		}
		
		try {
			if((BungeeCord.getInstance().getOnlineCount() >= PluginMeta.maxCount) && (!PlayerUtilities.isPremiumPlus(e.getConnection().getUniqueId().toString().replace("-", "")))){
				e.setCancelled(true);
				e.setCancelReason("�cDer Server ist voll!\n\n�6Kaufe dir Premium auf �estore.pvp-hub.net �6um immer\n�6auf den Server zu kommen");
			}
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
	}
	
}
