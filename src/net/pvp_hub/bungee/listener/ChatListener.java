package net.pvp_hub.bungee.listener;

import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.util.regex.Pattern;

import net.md_5.bungee.BungeeCord;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.event.ChatEvent;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.event.EventHandler;
import net.pvp_hub.bungee.PvPHubBungee;
import net.pvp_hub.bungee.api.PluginMeta;

public class ChatListener implements Listener {

	@EventHandler
	public void onChat(ChatEvent e){
		ProxiedPlayer p = (ProxiedPlayer)e.getSender();
		String message = e.getMessage();
		
		boolean cen = false;
		
		if(Pattern.compile(Pattern.quote("l2p"), Pattern.CASE_INSENSITIVE).matcher(message).find()){
			cen = true;
		}
		
		if(Pattern.compile(Pattern.quote("slut"), Pattern.CASE_INSENSITIVE).matcher(message).find()){
			cen = true;
		}
		
		if(Pattern.compile(Pattern.quote("hure"), Pattern.CASE_INSENSITIVE).matcher(message).find()){
			cen = true;
		}
		
		if(Pattern.compile(Pattern.quote("bitch"), Pattern.CASE_INSENSITIVE).matcher(message).find()){
			cen = true;
		}
		
		if(Pattern.compile(Pattern.quote("huso"), Pattern.CASE_INSENSITIVE).matcher(message).find()){
			cen = true;
		}
		
		if(Pattern.compile(Pattern.quote("www."), Pattern.CASE_INSENSITIVE).matcher(message).find()){
			cen = true;
		}
		
		if(Pattern.compile(Pattern.quote("wichs"), Pattern.CASE_INSENSITIVE).matcher(message).find()){
			cen = true;
		}
		
		if(Pattern.compile(Pattern.quote("fick"), Pattern.CASE_INSENSITIVE).matcher(message).find()){
			cen = true;
		}
		
		if(Pattern.compile(Pattern.quote("fuck"), Pattern.CASE_INSENSITIVE).matcher(message).find()){
			cen = true;
		}
		
		if(Pattern.compile(Pattern.quote("tits"), Pattern.CASE_INSENSITIVE).matcher(message).find()){
			cen = true;
		}
		
		if(p.hasPermission("pvphub.byPassChatModeration")){
			cen = false;
		}
		
		if(cen == true){
			e.setCancelled(true);
			p.sendMessage(new TextComponent("�7[�cStaffNotify�7] �cDeine Nachricht wurde zensiert."));
			p.sendMessage(new TextComponent("�7[�cStaffNotify�7] �cDie Admins wurden benachrichtigt."));
			
			for(ProxiedPlayer all : BungeeCord.getInstance().getPlayers()){
				if(all.hasPermission("pvphub.byPassChatModeration")){
					all.sendMessage(new TextComponent(PluginMeta.sn_prefix + "�4ChatFilter �7� �r" + PvPHubBungee.getRankColor(p) + p.getName() + "�8: " + e.getMessage()));
				}
			}
		}
	}
	
}