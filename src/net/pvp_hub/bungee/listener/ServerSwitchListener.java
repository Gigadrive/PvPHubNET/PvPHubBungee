package net.pvp_hub.bungee.listener;

import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.event.ServerSwitchEvent;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.event.EventHandler;
import net.pvp_hub.bungee.api.PartyManager;
import net.pvp_hub.bungee.api.PlayerUtilities;
import net.pvp_hub.bungee.api.PluginMeta;

public class ServerSwitchListener implements Listener {

	@EventHandler
	public void onServerSwitch(ServerSwitchEvent e){
		PlayerUtilities.updateServerLocation(e.getPlayer(), e.getPlayer().getServer().getInfo().getName());
		if(PartyManager.getParty(e.getPlayer().getName()) != null){
			ProxiedPlayer p = e.getPlayer();
			for(String members : PartyManager.getParty(p.getName()).getMembers()){
				ProxyServer.getInstance().getPlayer(members).connect(p.getServer().getInfo());
			}
			//PartyManager.getParty(e.getPlayer().getName()).sendMessage(PluginMeta.partyPrefix + "�aDie Party wurde auf den Server �e" + e.getPlayer().getServer().getInfo().getName() + " �averschoben.");
		}
		
		e.getPlayer().sendMessage(new TextComponent(PluginMeta.prefix + "�6Wechsle auf den Server " + e.getPlayer().getServer().getInfo().getName().toUpperCase() + "..."));
	}
	
}
