package net.pvp_hub.bungee.listener;

import net.md_5.bungee.BungeeCord;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.event.PlayerDisconnectEvent;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.event.EventHandler;
import net.pvp_hub.bungee.PvPHubBungee;
import net.pvp_hub.bungee.api.PlayerUtilities;
import net.pvp_hub.bungee.api.PluginMeta;

public class PlayerDisconnectListener implements Listener {

	@EventHandler
	public void onPlayerDisconnectEvent(PlayerDisconnectEvent e)
	  {
	  
		try {
			PlayerUtilities.updateOnlineStatus(e.getPlayer(), 0);
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
	    if (e.getPlayer().hasPermission("pvphub.rank.build")) {
		      for (ProxiedPlayer p : BungeeCord.getInstance().getPlayers()) {
		        if (p.hasPermission("staff.join")) {
		          p.sendMessage(new TextComponent(PluginMeta.sn_prefix + "�3" + e.getPlayer().getName() + " �7ist jetzt �4offline"));
		        }
		      }
		    }
	    
	    if (e.getPlayer().hasPermission("pvphub.rank.team")) {
		      for (ProxiedPlayer p : BungeeCord.getInstance().getPlayers()) {
		        if (p.hasPermission("staff.join")) {
		          p.sendMessage(new TextComponent(PluginMeta.sn_prefix + "�a" + e.getPlayer().getName() + " �7ist jetzt �4offline"));
		        }
		      }
		    }
	    
	    if (e.getPlayer().hasPermission("pvphub.rank.mod")) {
		      for (ProxiedPlayer p : BungeeCord.getInstance().getPlayers()) {
		        if (p.hasPermission("staff.join")) {
		          p.sendMessage(new TextComponent(PluginMeta.sn_prefix + "�e" + e.getPlayer().getName() + " �7ist jetzt �4offline"));
		        }
		      }
		    }
	    
	    if (e.getPlayer().hasPermission("pvphub.rank.dev")) {
		      for (ProxiedPlayer p : BungeeCord.getInstance().getPlayers()) {
		        if (p.hasPermission("staff.join")) {
		          p.sendMessage(new TextComponent(PluginMeta.sn_prefix + "�9" + e.getPlayer().getName() + " �7ist jetzt �4offline"));
		        }
		      }
		    }
	    
	    if (e.getPlayer().hasPermission("pvphub.rank.admin")) {
		      for (ProxiedPlayer p : BungeeCord.getInstance().getPlayers()) {
		        if (p.hasPermission("staff.join")) {
		          p.sendMessage(new TextComponent(PluginMeta.sn_prefix + "�c" + e.getPlayer().getName() + " �7ist jetzt �4offline"));
		        }
		      }
		    }
  
	
	if(e.getPlayer().hasPermission("staff.join")){
		PvPHubBungee.staff.remove(e.getPlayer());
	}
	
	
	  } }
