package net.pvp_hub.bungee.cmd;

import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;
import net.pvp_hub.bungee.PvPHubBungee;
import net.pvp_hub.bungee.api.PluginMeta;

public class PrivateMessage extends Command {

	public PrivateMessage(){
		super("msg", "pvphub.msg", new String[] { "msg", "whisper", "tell", "message", "t", "w" });
	}
	
	public void execute(CommandSender sender, String [] args){
		if(args.length < 2){
			sender.sendMessage(new TextComponent(PluginMeta.prefix + "�c/msg <Spieler> <Nachricht>"));
		} else {
			ProxiedPlayer p = ProxyServer.getInstance().getPlayer(args[0]);
			
			if(p == null){
				sender.sendMessage(new TextComponent(PluginMeta.prefix + "�cDieser Spieler ist nicht online."));
				return;
			} else {
				StringBuilder msgBuilder = new StringBuilder();
				for(int i = 1; i < args.length; i++) {
					msgBuilder.append(args[i]).append(" ");
				}
				
				String msg = msgBuilder.toString().trim();
				
				p.sendMessage(new TextComponent("�6[" + PvPHubBungee.getRankColor((ProxiedPlayer)sender) + sender.getName() + "�6->" + PvPHubBungee.getRankColor(p) + "mir�6] �7" + msg));
				sender.sendMessage(new TextComponent("�6[" + PvPHubBungee.getRankColor((ProxiedPlayer)sender) + "Ich �6-> " + PvPHubBungee.getRankColor(p) + p.getName() + "�6] �7" + msg));
		}
	}
	
}

}
