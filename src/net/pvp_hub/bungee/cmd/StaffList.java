package net.pvp_hub.bungee.cmd;

import net.md_5.bungee.BungeeCord;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;
import net.pvp_hub.bungee.PvPHubBungee;
import net.pvp_hub.bungee.api.PluginMeta;

public class StaffList extends Command {

	public StaffList(){
		super("stafflist", "staff.join", new String[] { "stafflist", "teamlist", "team", "staff" });
	}
	
	public void execute(CommandSender sender, String [] args){
		if(PvPHubBungee.staff.size() == 0){
			sender.sendMessage(new TextComponent(PluginMeta.sn_prefix + "�cEs ist derzeit kein Team-Mitglied online."));
		} else {
			sender.sendMessage(new TextComponent(PluginMeta.sn_prefix + "�8=== �eOnline-Team-Mitglieder �8==="));
			for(ProxiedPlayer staff : PvPHubBungee.staff){
				sender.sendMessage(new TextComponent(PluginMeta.sn_prefix + PvPHubBungee.getRankColor(staff) + staff.getName() + " �7(Server: " + staff.getServer().getInfo().getName() + "�7)"));
			}
			sender.sendMessage(new TextComponent(PluginMeta.sn_prefix + "�8=== �eOnline-Team-Mitglieder �8==="));
		}
	}
	
}
