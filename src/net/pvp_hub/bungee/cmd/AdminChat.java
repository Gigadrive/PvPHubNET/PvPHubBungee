package net.pvp_hub.bungee.cmd;

import java.util.Iterator;

import net.md_5.bungee.BungeeCord;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;
import net.pvp_hub.bungee.PvPHubBungee;
import net.pvp_hub.bungee.api.PluginMeta;

public class AdminChat extends Command {

	public AdminChat(){
		super("staffchat", "pvphub.adminchat", new String[] { "sc", "adminchat", "a", "ac" });
	}
	
	public void execute(CommandSender sender, String [] args){
		ProxiedPlayer p = (ProxiedPlayer)sender;
		
		if(args.length == 0){
			p.sendMessage(new TextComponent(PluginMeta.prefix + "�c/a <Nachricht>"));
		} else {
			String server = p.getServer().getInfo().getName();
			
			StringBuilder sb = new StringBuilder("");
	        for (int i = 0; i < args.length; i++) {
	          sb.append(args[i]).append(" ");
	        }
	        String s = sb.toString();
	        for (Iterator localIterator = BungeeCord.getInstance().getPlayers().iterator(); localIterator.hasNext();){
	          ProxiedPlayer all = (ProxiedPlayer)localIterator.next();
	          if (all.hasPermission("pvphub.adminchat")) {
	        	all.sendMessage(new TextComponent(PluginMeta.sn_prefix_chat + "�e" + server + "�8|" + PvPHubBungee.getRankColor(p) + p.getName() + "�f: " + s));
	          }
	        }
		}
	}
	
}
