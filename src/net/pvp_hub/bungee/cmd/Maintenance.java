package net.pvp_hub.bungee.cmd;

import net.md_5.bungee.BungeeCord;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;
import net.pvp_hub.bungee.PvPHubBungee;
import net.pvp_hub.bungee.api.PluginMeta;

public class Maintenance extends Command {

	public Maintenance(){
		super("maintenance", "pvphub.activateMaintenance", new String[] { "maintenance", "m" });
	}
	
	public void execute(CommandSender sender, String [] args){
		ProxiedPlayer p = (ProxiedPlayer)sender;
		
		if(PvPHubBungee.isInMaintenance){
			PvPHubBungee.isInMaintenance = false;
			for(ProxiedPlayer all : BungeeCord.getInstance().getPlayers()){
				if(all.hasPermission("staff.join")){
					all.sendMessage(new TextComponent(PluginMeta.prefix + "�aDer Wartungsmodus wurde von " + PvPHubBungee.getRankColor(p) + p.getName() + " �adeaktiviert"));
				}
			}
		} else {
			PvPHubBungee.isInMaintenance = true;
			for(ProxiedPlayer all : BungeeCord.getInstance().getPlayers()){
				if(all.hasPermission("staff.join")){
					all.sendMessage(new TextComponent(PluginMeta.prefix + "�aDer Wartungsmodus wurde von " + PvPHubBungee.getRankColor(p) + p.getName() + " �aaktiviert"));
				}
				
				if(!all.hasPermission("pvphub.joinMaintenance")){
					all.disconnect(new TextComponent("�cDer Server befindet sich nun im Wartungsmodus."));
				}
			}
		}
	}
	
}
