package net.pvp_hub.bungee.cmd;

import net.md_5.bungee.BungeeCord;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;
import net.pvp_hub.bungee.PvPHubBungee;
import net.pvp_hub.bungee.api.PluginMeta;

public class Brotkasten extends Command {

	public Brotkasten(){
		super("broadcast", null, new String[] { "say", "bc", "alert", "shout", "sys" });
	}
	
	public void execute(CommandSender sender, String [] args){
		ProxiedPlayer p = (ProxiedPlayer)sender;
		
		if(args.length == 0){
			p.sendMessage(new TextComponent(PluginMeta.prefix + "�c/broadcast <Nachricht>"));
		} else {
			StringBuilder sb = new StringBuilder();
			for (int i = 0; i < args.length; i++) {
			sb.append(" ").append(args[i]);
			}
			
			String nachricht = sb.toString().substring(1);
			
			nachricht = nachricht.replaceAll("&a", "�a");
			nachricht = nachricht.replaceAll("&c", "�c");
			nachricht = nachricht.replaceAll("&4", "�4");
			nachricht = nachricht.replaceAll("&6", "�6");
			nachricht = nachricht.replaceAll("&l", "�l");
		    nachricht = nachricht.replaceAll("&o", "�o");
		    nachricht = nachricht.replaceAll("&9", "�9");
		    nachricht = nachricht.replaceAll("&0", "�0");
		    nachricht = nachricht.replaceAll("&8", "�8");
		    nachricht = nachricht.replaceAll("&9", "�9");
		    nachricht = nachricht.replaceAll("&d", "�d");
		    nachricht = nachricht.replaceAll("&e", "�e");
		    nachricht = nachricht.replaceAll("&3", "�3");
		    nachricht = nachricht.replaceAll("&b", "�b");
		    nachricht = nachricht.replaceAll("&7", "�7");
		    nachricht = nachricht.replaceAll("&1", "�1");
		    nachricht = nachricht.replaceAll("&2", "�2");
		    nachricht = nachricht.replaceAll("&5", "�5");
		    nachricht = nachricht.replaceAll("&f", "�f");
		    nachricht = nachricht.replaceAll("&r", "�r");
		    
		    final String finalMessage = "�c�lBROADCAST �r�8| " + PvPHubBungee.getRankColor(p) + p.getName() + " �8� �f" + nachricht;
		    BungeeCord.getInstance().broadcast(finalMessage);
		}
	}
	
}
