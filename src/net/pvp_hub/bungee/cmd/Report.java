package net.pvp_hub.bungee.cmd;

import java.util.Iterator;

import net.md_5.bungee.BungeeCord;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;
import net.pvp_hub.bungee.PvPHubBungee;
import net.pvp_hub.bungee.api.PluginMeta;

public class Report extends Command {

	public Report(){
		super("report", null, new String[] { "report", "ticket", "helpop" });
	}
	
	public void execute(CommandSender sender, String [] args){
		ProxiedPlayer p = (ProxiedPlayer)sender;
		
		if(args.length == 0){
			p.sendMessage(new TextComponent(PluginMeta.prefix + "�c/report <Nachricht>"));
		} else {
			String server = p.getServer().getInfo().getName();
			
			sender.sendMessage(new TextComponent(PluginMeta.sn_prefix + "�aDeine Nachricht wurde einem Team-Mitglied mitgeteilt!"));
			StringBuilder sb = new StringBuilder("");
			for (int i = 0; i < args.length; i++) {
				sb.append(args[i]).append(" ");
			}
			String s = sb.toString();
			for (java.util.Iterator<ProxiedPlayer> localIterator = BungeeCord.getInstance().getPlayers().iterator(); localIterator.hasNext();){
				ProxiedPlayer all = (ProxiedPlayer)localIterator.next();
				if(all.hasPermission("pvphub.reportreceive")) {
					all.sendMessage(new TextComponent(PluginMeta.sn_prefix_report + "�e" + server + "�8|" + PvPHubBungee.getRankColor(p) + p.getName() + "�f: " + s));
				}
			}
		}
	}
	
}
