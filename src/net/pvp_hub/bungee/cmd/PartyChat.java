package net.pvp_hub.bungee.cmd;

import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;
import net.pvp_hub.bungee.PvPHubBungee;
import net.pvp_hub.bungee.api.PartyManager;
import net.pvp_hub.bungee.api.PluginMeta;

public class PartyChat extends Command {

	public PartyChat(){
		super("pchat", null, new String[] { "partychat", "pchat" });
	}
	
	public void execute(CommandSender sender, String [] args){
		if((sender instanceof ProxiedPlayer)){
			ProxiedPlayer p = (ProxiedPlayer)sender;
			if(PvPHubBungee.getParty.containsKey(p.getName())){
				StringBuilder b = new StringBuilder();
	            for (int i = 0; i < args.length; i++) {
	              b.append(args[i]).append(" ");
	            }
	            if (PartyManager.getParty(p.getName()).getOwner() == p) {
	              PartyManager.getParty(p.getName()).sendMessage("�3�lPARTY-CHAT | �r�8<[�eParty-Owner�8] " + PvPHubBungee.getRankColor(p) + p.getName() + "�8> �f" + b.toString());
	            } else {
	              PartyManager.getParty(p.getName()).sendMessage("�3�lPARTY-CHAT | �r�8<[�7Party-Mitglied�8] " + PvPHubBungee.getRankColor(p) + p.getName() + "�8> �f" + b.toString());
	            }
			} else {
				p.sendMessage(new TextComponent(PluginMeta.partyPrefix + "�cDu bist nicht in einer Party."));
			}
		}
	}
	
}
