package net.pvp_hub.bungee.cmd;

import net.md_5.bungee.BungeeCord;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;
import net.pvp_hub.bungee.api.PluginMeta;

public class Find extends Command {

	public Find(){
		super("find", "bungeecord.command.find", new String[] { "find", "whereis" });
	}
	
	public void execute(CommandSender sender, String [] args){
		ProxiedPlayer p = (ProxiedPlayer)sender;
		
		if(args.length == 0){
			p.sendMessage(new TextComponent(PluginMeta.prefix + "�c/whereis <Player>"));
		} else {
			if(BungeeCord.getInstance().getPlayer(args[0]) == null){
				p.sendMessage(new TextComponent(PluginMeta.prefix + "�c" + args[0] + " ist offline."));
			} else {
				String server = BungeeCord.getInstance().getPlayer(args[0]).getServer().getInfo().getName();
				p.sendMessage(new TextComponent(PluginMeta.prefix + "�aIst online auf dem Server �e" + server));
			}
		}
	}
	
}
