package net.pvp_hub.bungee.cmd;

import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.plugin.Command;
import net.pvp_hub.bungee.api.PluginMeta;

public class Help extends Command {

	public Help(){
		super("help", null, new String[] { "help", "?", "helpme" });
	}
	
	public void execute(CommandSender sender, String [] args){
		sender.sendMessage(new TextComponent(PluginMeta.noperms));
	}
	
}
