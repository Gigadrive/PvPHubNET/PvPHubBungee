package net.pvp_hub.bungee.cmd;

import net.md_5.bungee.BungeeCord;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.config.ServerInfo;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;
import net.pvp_hub.bungee.api.PlayerUtilities;
import net.pvp_hub.bungee.api.PluginMeta;

public class Follow extends Command {

	public Follow(){
		super("follow", "staff.join", new String[] { "follow", "goto" });
	}
	
	public void execute(CommandSender sender, String [] args){
		ProxiedPlayer p = (ProxiedPlayer)sender;
		
		if(args.length == 0){
			p.sendMessage(new TextComponent(PluginMeta.prefix + "�c/follow <Player>"));
		} else {
			if(PlayerUtilities.isOnline(args[0])){
				ServerInfo info = BungeeCord.getInstance().getPlayer(args[0]).getServer().getInfo();
				p.connect(info);
			} else {
				p.sendMessage(new TextComponent(PluginMeta.prefix + "�c" + args[0] + " ist nicht online."));
			}
		}
		
	}
	
}
