package net.pvp_hub.bungee.cmd;

import java.util.ArrayList;
import java.util.Arrays;

import net.md_5.bungee.BungeeCord;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.config.ServerInfo;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;
import net.pvp_hub.bungee.PvPHubBungee;
import net.pvp_hub.bungee.api.PlayerUtilities;
import net.pvp_hub.bungee.api.PluginMeta;

public class Friend extends Command {

	public Friend(){
		super("friend", null, new String[] { "friend", "friends", "f" });
	}
	
	public void execute(CommandSender sender, String [] args){
		if((sender instanceof ProxiedPlayer)){
			ProxiedPlayer p = (ProxiedPlayer)sender;
			
			if(args.length == 0){
				p.sendMessage(new TextComponent(PluginMeta.prefixFriend + "�e/f add <Player> �7| �aF�ge einen Spieler als Freund hinzu"));
				p.sendMessage(new TextComponent(PluginMeta.prefixFriend + "�e/f accept <Player> �7| �aEine Freundschaftsanfrage akzeptieren"));
				p.sendMessage(new TextComponent(PluginMeta.prefixFriend + "�e/f deny <Player> �7| �aEine Freundschaftsanfrage ablehnen"));
				p.sendMessage(new TextComponent(PluginMeta.prefixFriend + "�e/f remove|delete <Player> �7| �aEinen Freund entfernen"));
				p.sendMessage(new TextComponent(PluginMeta.prefixFriend + "�e/f follow|jump <Player> �7| �aFolge einem Spieler auf einen Server"));
				p.sendMessage(new TextComponent(PluginMeta.prefixFriend + "�e/f list �7| �aFreunde auflisten"));
				p.sendMessage(new TextComponent(PluginMeta.prefixFriend + "�e/f requests �7| �aFreundschaftsanfragen auflisten"));
			} else if(args[0].equalsIgnoreCase("add")){
				if(args.length == 1){
					try {
						if(PlayerUtilities.existInDatabase(args[0])){
							if(PlayerUtilities.hasFriends(args[0]) == 1){
								if(PlayerUtilities.isOnline(args[0])){
									p.sendMessage(new TextComponent(PluginMeta.prefixFriend + "�aNeue Freundschaftsanfrage!"));
									p.sendMessage(new TextComponent(PluginMeta.prefixFriend + PvPHubBungee.getRankColor(BungeeCord.getInstance().getPlayer(args[0])) + args[0] + " �am�chte mit dir befreundet sein!"));
									p.sendMessage(new TextComponent(PluginMeta.prefixFriend + "�e/friend accept " + args[0] + " �2um zu akzeptieren"));
									p.sendMessage(new TextComponent(PluginMeta.prefixFriend + "�e/friend deny " + args[0] + " �4um abzulehnen."));
								}
								
								PlayerUtilities.createNewFriendRequest(PlayerUtilities.getPvPHubID(p.getName()), PlayerUtilities.getPvPHubID(args[0]));
							} else {
								p.sendMessage(new TextComponent(PluginMeta.prefixFriend + "�cDieser Spieler akzeptiert keine Anfragen."));
							}
						} else {
							p.sendMessage(new TextComponent(PluginMeta.prefixFriend + "�cDer Spieler " + args[0] + " hat noch nie auf PvP-Hub gespielt."));
						}
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				} else {
					p.sendMessage(new TextComponent(PluginMeta.prefixFriend + "�e/f add <Player> �7| �aF�ge einen Spieler als Freund hinzu"));
				}
			} else if(args[0].equalsIgnoreCase("accept")){
				if(args.length == 1){
					try {
						if(PlayerUtilities.hasFriendRequest(PlayerUtilities.getPvPHubID(args[0]), PlayerUtilities.getPvPHubID(p.getName()))){
							if(PlayerUtilities.isOnline(args[0])){
								BungeeCord.getInstance().getPlayer(args[0]).sendMessage(new TextComponent(PluginMeta.prefixFriend + PvPHubBungee.getRankColor(p) + p.getName() + " �ahat deine Freundschaftsanfrage akzeptiert."));
							}
							p.sendMessage(new TextComponent(PluginMeta.prefixFriend + "�aDu bist nun mit " + PvPHubBungee.getRankColor(BungeeCord.getInstance().getPlayer(args[0])) + args[0] + " �abefreundet."));
							PlayerUtilities.deleteFriendRequest(PlayerUtilities.getPvPHubID(args[0]), PlayerUtilities.getPvPHubID(p.getName()));
							
						} else {
							p.sendMessage(new TextComponent(PluginMeta.prefixFriend + "�cDu hast keine Freundschaftsanfrage von " + args[0]));
						}
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				} else {
					p.sendMessage(new TextComponent(PluginMeta.prefixFriend + "�e/f accept <Player> �7| �aEine Freundschaftsanfrage akzeptieren"));
				}
			} else if(args[0].equalsIgnoreCase("deny")){
				if(args.length == 1){
					
				} else {
					p.sendMessage(new TextComponent(PluginMeta.prefixFriend + "�e/f deny <Player> �7| �aEine Freundschaftsanfrage ablehnen"));
				}
			} else if(args[0].equalsIgnoreCase("remove") || args[0].equalsIgnoreCase("delete")){
				if(args.length == 1){
					
				} else {
					p.sendMessage(new TextComponent(PluginMeta.prefixFriend + "�e/f remove|delete <Player> �7| �aEinen Freund entfernen"));
				}
			} else if(args[0].equalsIgnoreCase("follow") || args[0].equalsIgnoreCase("jump")){
				if(args.length == 1){
					try {
						if(PlayerUtilities.isFriendedWith(PlayerUtilities.getPvPHubID(p.getName()), PlayerUtilities.getPvPHubID(args[0]))){
							if(PlayerUtilities.isOnline(args[0])){
								ServerInfo info = BungeeCord.getInstance().getPlayer(args[0]).getServer().getInfo();
								p.connect(info);
							}
						} else {
							p.sendMessage(new TextComponent(PluginMeta.prefixFriend + "�cDu bist nicht mit " + args[0] + " befreundet."));
						}
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				} else {
					p.sendMessage(new TextComponent(PluginMeta.prefixFriend + "�e/f follow|jump <Player> �7| �aFolge einem Spieler auf einen Server"));
				}
			} else if(args[0].equalsIgnoreCase("list")){
				try {
					p.sendMessage(new TextComponent(PluginMeta.prefixFriend + PlayerUtilities.getFriends(PlayerUtilities.getPvPHubID(p.getName()))));
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			} else if(args[0].equalsIgnoreCase("requests")){
				p.sendMessage(new TextComponent(PluginMeta.prefixFriend + "�e/f requests �7| �aFreundschaftsanfragen auflisten"));
			} else {
				p.sendMessage(new TextComponent(PluginMeta.prefixFriend + "�e/f add <Player> �7| �aF�ge einen Spieler als Freund hinzu"));
				p.sendMessage(new TextComponent(PluginMeta.prefixFriend + "�e/f accept <Player> �7| �aEine Freundschaftsanfrage akzeptieren"));
				p.sendMessage(new TextComponent(PluginMeta.prefixFriend + "�e/f deny <Player> �7| �aEine Freundschaftsanfrage ablehnen"));
				p.sendMessage(new TextComponent(PluginMeta.prefixFriend + "�e/f remove|delete <Player> �7| �aEinen Freund entfernen"));
				p.sendMessage(new TextComponent(PluginMeta.prefixFriend + "�e/f follow|jump <Player> �7| �aFolge einem Spieler auf einen Server"));
				p.sendMessage(new TextComponent(PluginMeta.prefixFriend + "�e/f list �7| �aFreunde auflisten"));
				p.sendMessage(new TextComponent(PluginMeta.prefixFriend + "�e/f requests �7| �aFreundschaftsanfragen auflisten"));
			}
			
		} else {
			sender.sendMessage(new TextComponent(PluginMeta.noplayer));
		}
	}
	
}
