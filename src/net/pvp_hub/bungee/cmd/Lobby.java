package net.pvp_hub.bungee.cmd;

import net.md_5.bungee.BungeeCord;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;
import net.pvp_hub.bungee.api.PluginMeta;

public class Lobby extends Command {

	public Lobby(){
		super("lobby", null, new String[] { "lobby", "hub", "quit", "leave", "l", "q" });
	}
	
	public void execute(CommandSender sender, String [] args){
		ProxiedPlayer p = (ProxiedPlayer)sender;
		
		p.sendMessage(new TextComponent(PluginMeta.prefix + "žaTeleportiere dich auf einen Lobby Server.."));
		p.connect(ProxyServer.getInstance().getServerInfo("lobby"));
	}
	
}
