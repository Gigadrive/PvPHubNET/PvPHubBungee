package net.pvp_hub.bungee.cmd;

import java.util.ArrayList;

import net.md_5.bungee.BungeeCord;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;
import net.pvp_hub.bungee.PvPHubBungee;
import net.pvp_hub.bungee.api.BungeeParty;
import net.pvp_hub.bungee.api.PartyManager;
import net.pvp_hub.bungee.api.PluginMeta;

public class Party extends Command {

	public Party(){
		super("party", null, new String[] { "party", "p" });
	}
	
	public void execute(CommandSender sender, String [] args){
		if((sender instanceof ProxiedPlayer)){
			ProxiedPlayer p = (ProxiedPlayer)sender;
			if(args.length == 0){
				if(PvPHubBungee.getParty.containsKey(p.getName())){
					p.sendMessage(new TextComponent(PluginMeta.partyPrefix + "�cDu bist bereits in einer Party."));
				} else {
					PvPHubBungee.party.put(p.getName(), new BungeeParty(p, new ArrayList()));
					p.sendMessage(new TextComponent(PluginMeta.partyPrefix + "�aParty erstellt."));
				}
			} else if(args[0].equalsIgnoreCase("list")){
				if(PvPHubBungee.getParty.containsKey(p.getName())){
					p.sendMessage(new TextComponent(PluginMeta.partyPrefix + "=== �eMitglieder �6==="));
					
					for(String pn : PartyManager.getParty(p.getName()).getMembers()){
						ProxiedPlayer pl = BungeeCord.getInstance().getPlayer(pn);
						
						p.sendMessage(new TextComponent(PluginMeta.partyPrefix + PvPHubBungee.getRankColor(pl) + pn));
					}
					
					p.sendMessage(new TextComponent(PluginMeta.partyPrefix + "=== �eMitglieder �6==="));
				} else {
					p.sendMessage(new TextComponent(PluginMeta.partyPrefix + "�cDu bist nicht in einer Party."));
				}
			} else if(args[0].equalsIgnoreCase("kick")){
				if(PvPHubBungee.getParty.containsKey(p.getName())){
					if(PartyManager.getParty(p.getName()).getOwner() == p){
						PartyManager.removeFromParty(args[1], PartyManager.getParty(p.getName()));
						PartyManager.getParty(p.getName()).sendMessage(PluginMeta.partyPrefix + PvPHubBungee.getRankColor(BungeeCord.getInstance().getPlayer(args[1])) + args[1] + " �6wurde aus der Party gekickt.");
						BungeeCord.getInstance().getPlayer(args[1]).sendMessage(new TextComponent(PluginMeta.partyPrefix + "�cDu wurdest aus der Party gekickt."));
						PvPHubBungee.getParty.remove(args[1]);
					}
				} else {
					p.sendMessage(new TextComponent(PluginMeta.partyPrefix + "�cDu bist nicht in einer Party."));
				}
			} else if(args[0].equalsIgnoreCase("leave")){
				if(PvPHubBungee.getParty.containsKey(p.getName())){
					if (PartyManager.getParty(p.getName()).getOwner() == p)
		            {
		              for (String members : PartyManager.getParty(p.getName()).getMembers())
		              {
		                PartyManager.getParty(p.getName()).sendMessage(PluginMeta.partyPrefix + "�cDie Party wurde aufgel�st.");
		                PvPHubBungee.getParty.remove(members);
		              }
		              PvPHubBungee.party.remove(p.getName());
		            }
		            else
		            {
		              PartyManager.removeFromParty(p.getName(), PartyManager.getParty(p.getName()));
		              PartyManager.getParty(p.getName()).sendMessage(PluginMeta.partyPrefix + PvPHubBungee.getRankColor(p) + p.getName() + " �6hat die Party verlassen.");
		              p.sendMessage(new TextComponent(PluginMeta.partyPrefix + "�cDu hast die Party verlassen."));
		              PvPHubBungee.getParty.remove(p.getName());
		            }
				} else {
					p.sendMessage(new TextComponent(PluginMeta.partyPrefix + "�cDu bist nicht in einer Party."));
				}
			} else if(args[0].equalsIgnoreCase("accept")){
				if(PvPHubBungee.inviteParty.containsKey(p.getName())){
		            if (PvPHubBungee.party.containsKey(PvPHubBungee.inviteParty.get(p.getName()))){
		              if (PartyManager.getParty((String)PvPHubBungee.inviteParty.get(p.getName())).getMembers().size() == 7) {
		                p.sendMessage(new TextComponent(PluginMeta.partyPrefix + "�cDie Party ist voll."));
		              } else {
		                PartyManager.addToParty(p.getName(), PartyManager.getParty((String)PvPHubBungee.inviteParty.get(p.getName())));
			            PvPHubBungee.getParty.put(p.getName(), args[1]);
			            PartyManager.getParty(args[1]).sendMessage(PluginMeta.partyPrefix + PvPHubBungee.getRankColor(p) + p.getName() + " �6hat die Party betreten.");
		              }
		            } else {
		              p.sendMessage(new TextComponent(PluginMeta.partyPrefix + "�cDiese Party existiert nicht mehr."));
		            }
		          } else {
		        	  p.sendMessage(new TextComponent("ne"));
		          }
			} else if(args[0].equalsIgnoreCase("invite")){
				if(PvPHubBungee.getParty.containsKey(p.getName())){
					if (PartyManager.getParty(p.getName()).getOwner() == p){
		                if (ProxyServer.getInstance().getPlayer(args[1]) == null){
		                  p.sendMessage(new TextComponent(PluginMeta.partyPrefix + "�cDieser Spieler ist nicht online."));
		                } else {
		                  ProxiedPlayer t = ProxyServer.getInstance().getPlayer(args[1]);
		                  if (PvPHubBungee.getParty.containsKey(t.getName())){
		                    p.sendMessage(new TextComponent(PluginMeta.partyPrefix + PvPHubBungee.getRankColor(t) + t.getName() + " �cist bereits in einer Party."));
		                  } else {
		                    PartyManager.inviteToParty(t.getName(), PartyManager.getParty(p.getName()));
		                    t.sendMessage(new TextComponent(PluginMeta.partyPrefix + PvPHubBungee.getRankColor(p) + p.getName() + " �ahat dich in eine Party eingeladen."));
		                    t.sendMessage(new TextComponent(PluginMeta.partyPrefix + "�aBenutze �e/party accept " + p.getName() + " �aum der Party beizutreten."));
		                    p.sendMessage(new TextComponent(PluginMeta.partyPrefix + "�aDu hast " + PvPHubBungee.getRankColor(t) + t.getName() + " �aeingeladen."));
		                  }
		                }
		              }
		              else {
		                p.sendMessage(new TextComponent(PluginMeta.partyPrefix + "�cDu bist nicht der Party Ersteller."));
		              }
				} else {
					p.sendMessage(new TextComponent(PluginMeta.partyPrefix + "�cDu bist nicht in einer Party."));
				}
			} else if(args[0].equalsIgnoreCase("chat")){
				if(PvPHubBungee.getParty.containsKey(p.getName())){
					StringBuilder b = new StringBuilder();
		            for (int i = 0; i < args.length; i++) {
		              b.append(args[i]).append(" ");
		            }
		            if (PartyManager.getParty(p.getName()).getOwner() == p) {
		              PartyManager.getParty(p.getName()).sendMessage("�3�lPARTY-CHAT | �r�8<[�eParty-Owner�8] " + PvPHubBungee.getRankColor(p) + p.getName() + "�8> �f" + b.toString());
		            } else {
		              PartyManager.getParty(p.getName()).sendMessage("�3�lPARTY-CHAT | �r�8<[�7Party-Mitglied�8] " + PvPHubBungee.getRankColor(p) + p.getName() + "�8> �f" + b.toString());
		            }
				} else {
					p.sendMessage(new TextComponent(PluginMeta.partyPrefix + "�cDu bist nicht in einer Party."));
				}
			} else if(args[0].equalsIgnoreCase("help")){
				p.sendMessage(new TextComponent(PluginMeta.partyPrefix + "=== �eParty �6==="));
				p.sendMessage(new TextComponent(PluginMeta.partyPrefix + "�e/party �8 - �aEine Party erstellen"));
				p.sendMessage(new TextComponent(PluginMeta.partyPrefix + "�e/party leave �8 - �aEine Party verlassen"));
				p.sendMessage(new TextComponent(PluginMeta.partyPrefix + "�e/party accept �8 - �aAkzeptiert eine Party Einladung"));
				p.sendMessage(new TextComponent(PluginMeta.partyPrefix + "�e/party kick <Spieler>�8 - �aKickt einen Spieler aus einer Party."));
				p.sendMessage(new TextComponent(PluginMeta.partyPrefix + "�e/party invite <Spieler>�8 - �aL�dt einen Spieler in eine Party ein."));
				p.sendMessage(new TextComponent(PluginMeta.partyPrefix + "�e/party list�8 - �aZeigt eine Liste der Party Mitglieder"));
				p.sendMessage(new TextComponent(PluginMeta.partyPrefix + "�e/party help�8 - �aZeigt diesen Hilfe-Text"));
				p.sendMessage(new TextComponent(PluginMeta.partyPrefix + "�e/pchat <Nachricht>�8 - �aSendet eine Nachricht an die Party Mitglieder."));
				p.sendMessage(new TextComponent(PluginMeta.partyPrefix + "=== �eParty �6==="));
			} else {
				p.sendMessage(new TextComponent(PluginMeta.partyPrefix + "=== �eParty �6==="));
				p.sendMessage(new TextComponent(PluginMeta.partyPrefix + "�e/party �8 - �aEine Party erstellen"));
				p.sendMessage(new TextComponent(PluginMeta.partyPrefix + "�e/party leave �8 - �aEine Party verlassen"));
				p.sendMessage(new TextComponent(PluginMeta.partyPrefix + "�e/party accept �8 - �aAkzeptiert eine Party Einladung"));
				p.sendMessage(new TextComponent(PluginMeta.partyPrefix + "�e/party kick <Spieler>�8 - �aKickt einen Spieler aus einer Party."));
				p.sendMessage(new TextComponent(PluginMeta.partyPrefix + "�e/party invite <Spieler>�8 - �aL�dt einen Spieler in eine Party ein."));
				p.sendMessage(new TextComponent(PluginMeta.partyPrefix + "�e/party list�8 - �aZeigt eine Liste der Party Mitglieder"));
				p.sendMessage(new TextComponent(PluginMeta.partyPrefix + "�e/party help�8 - �aZeigt diesen Hilfe-Text"));
				p.sendMessage(new TextComponent(PluginMeta.partyPrefix + "�e/pchat <Nachricht>�8 - �aSendet eine Nachricht an die Party Mitglieder."));
				p.sendMessage(new TextComponent(PluginMeta.partyPrefix + "=== �eParty �6==="));
			}
		}
	}
	
}
