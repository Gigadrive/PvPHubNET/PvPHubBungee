package net.pvp_hub.bungee.api;

import net.pvp_hub.bungee.PvPHubBungee;

public class PartyManager {

	private static String player;
	private static BungeeParty party;
	
	public static BungeeParty getParty(String player){
		PartyManager.player = player;
		
		return (BungeeParty)PvPHubBungee.party.get(PvPHubBungee.getParty.get(player));
	}
	
	public static void inviteToParty(String player, BungeeParty party){
		PartyManager.player = player;
		PartyManager.party = party;
		
		PvPHubBungee.inviteParty.put(player, party.getOwner().getName());
	}
	
	public static void removeFromParty(String player, BungeeParty party){
		PartyManager.player = player;
		PartyManager.party = party;
		
		party.getMembers().remove(player);
	}
	
	public static void addToParty(String player, BungeeParty party){
		PartyManager.player = player;
		PartyManager.party = party;
		
		party.getMembers().add(player);
	}
	
}
