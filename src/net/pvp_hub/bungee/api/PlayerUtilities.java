package net.pvp_hub.bungee.api;

import java.awt.List;

import net.md_5.bungee.BungeeCord;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.pvp_hub.bungee.PvPHubBungee;
import tk.theakio.sql.HandlerStorage;
import tk.theakio.sql.QueryResult;

public class PlayerUtilities {
	
	public static String getUUIDFromOfflinePlayer(String p) throws Exception {
		String uuid = null;

		QueryResult qr = HandlerStorage.getHandler("main").executeQuery("SELECT * FROM users WHERE name='" + p + "'");
		qr.rs.last();
		
		if(qr.rs.getRow() != 0){
			uuid = qr.rs.getString("uuid");
		}
		HandlerStorage.getHandler("main").closeRecources(qr.rs, qr.st);
		
		return uuid;
	}
	
	public static void updateOnlineStatus(ProxiedPlayer player, int isOnline) throws Exception {

		try {
			HandlerStorage.getHandler("main").execute("UPDATE `PvPHub`.`users` SET `is_online` = '" + isOnline + "' WHERE `users`.`uuid` = '" + PlayerUtilities.getUUID(player) + "';");
		} catch (Exception e){
		  	e.printStackTrace();
		}
	}
	
	@SuppressWarnings("deprecation")
	public static void sendDebugMessage(String message){
		for(ProxiedPlayer all : BungeeCord.getInstance().getPlayers()){
			if(PvPHubBungee.debuggers.contains(all.getName())){
				all.sendMessage("�7[�cStaffNotify�c|�9Debug (Dev)�7] �c" + message);
			}
		}
	}
	
	public static boolean isPremiumPlus(String uuid) throws Exception {
		boolean b = false;
		int rank = 0;
		QueryResult qr = HandlerStorage.getHandler("main").executeQuery("SELECT * FROM users WHERE uuid='" + uuid + "'");
		qr.rs.last();
		
		if(qr.rs.getRow() != 0){
			rank = qr.rs.getInt("rank");
			
			if(rank == 0){
				b = false;
			} else {
				b = true;
			}
		}
		HandlerStorage.getHandler("main").closeRecources(qr.rs, qr.st);
		
		return b;
	}
	
	public static void updateServerLocation(ProxiedPlayer p, String server){
		try {
			HandlerStorage.getHandler("main").execute("UPDATE `PvPHub`.`users` SET `server` = '" + server + "' WHERE `users`.`uuid` = '" + getUUID(p) + "';");
		} catch (Exception e){
		  	e.printStackTrace();
		}
	}

	public static int hasFriends(String p) throws Exception {
		int l = 0;
		
		String uuid;
		if(isOnline(p)){
			uuid = BungeeCord.getInstance().getPlayer(p).getUniqueId().toString().replace("-", "");
		} else {
			uuid = getUUIDFromOfflinePlayer(p);
		}
		
		QueryResult qr = HandlerStorage.getHandler("main").executeQuery("SELECT * FROM users WHERE uuid='" + uuid  + "'");
		qr.rs.last();
		
		if(qr.rs.getRow() != 0){
			l = qr.rs.getInt("setting_allowFriendRequests");
		}
		HandlerStorage.getHandler("main").closeRecources(qr.rs, qr.st);
		
		return l;
	}
	
	public static boolean existInDatabase(String name) throws Exception {
		boolean b = false;
		
		QueryResult qr = HandlerStorage.getHandler("main").executeQuery("SELECT * FROM users WHERE name='" + name + "'");
		qr.rs.last();
		
		if(qr.rs.getRow() != 0){
			b = true;
		}
		HandlerStorage.getHandler("main").closeRecources(qr.rs, qr.st);
		
		return b;
	}
	
	public static int getPvPHubID(String name) throws Exception {
		int b = 0;
		
		String uuid;
		if(isOnline(name)){
			uuid = BungeeCord.getInstance().getPlayer(name).getUniqueId().toString().replace("-", "");
		} else {
			uuid = getUUIDFromOfflinePlayer(name);
		}
		
		QueryResult qr = HandlerStorage.getHandler("main").executeQuery("SELECT * FROM users WHERE name='" + name + "'");
		qr.rs.last();
		
		if(qr.rs.getRow() != 0){
			b = qr.rs.getInt("id");
		}
		HandlerStorage.getHandler("main").closeRecources(qr.rs, qr.st);
		
		return b;
	}
	
	public static String getUUID(ProxiedPlayer p){
		return p.getUniqueId().toString().replace("-", "");
	}
	
	public static boolean isOnline(String name) {
		boolean b = false;
		
		if(BungeeCord.getInstance().getPlayer(name) == null){
			b = false;
		} else {
			b = true;
		}
		
		return b;
	}
	
	public static void createNewFriendRequest(int from, int to) throws Exception {
		HandlerStorage.getHandler("main").execute("INSERT INTO friendRequests (`from` ,`to`)VALUES (" + from + ", " + to + ");");
	}
	
	public static void deleteFriendRequest(int from, int to) throws Exception {
		HandlerStorage.getHandler("main").execute("DELETE FROM `friendRequests` WHERE `to` = " + to);
	}
	
	public static boolean hasFriendRequest(int from, int to) throws Exception {
		boolean b = false;
		
		QueryResult qr = HandlerStorage.getHandler("main").executeQuery("SELECT * FROM friendRequests WHERE to=" + to + " AND from=" + from);
		qr.rs.last();
		
		if(qr.rs.getRow() != 0){
			b = true;
		}
		HandlerStorage.getHandler("main").closeRecources(qr.rs, qr.st);
		
		return b;
	}
	
	public static String getNameFromID(int id) throws Exception {
		String name = null;
		
		QueryResult qr = HandlerStorage.getHandler("main").executeQuery("SELECT * FROM users WHERE id=" + id);
		qr.rs.last();
		if(qr.rs.getRow() != 0){
			name = qr.rs.getString("name");
		}
		
		return name;
	}
	
	public static String getFriends(int id) throws Exception {
		String friends = null;
		String name = getNameFromID(id);
		
		String uuid;
		if(isOnline(name)){
			uuid = BungeeCord.getInstance().getPlayer(name).getUniqueId().toString().replace("-", "");
		} else {
			uuid = getUUIDFromOfflinePlayer(name);
		}
		
		QueryResult qr = HandlerStorage.getHandler("main").executeQuery("SELECT * FROM users WHERE uuid='" + uuid + "'");
		qr.rs.last();
		
		if(qr.rs.getRow() != 0){
			friends = qr.rs.getString("friends");
		}
		HandlerStorage.getHandler("main").closeRecources(qr.rs, qr.st);
		
		return friends;
	}
	
	/*public static List getFriendRequests(int id) throws Exception {
		List requests = new List();
		
		QueryResult qr = HandlerStorage.getHandler("main").executeQuery("SELECT * FROM users WHERE to=" + id + "");
		qr.rs.last();
		
		if(qr.rs.getRow() != 0){
			friends = qr.rs.getString("friends");
			//for(qr.rs.)
		}
		HandlerStorage.getHandler("main").closeRecources(qr.rs, qr.st);
		
		return friends;
	}*/
	
	public static boolean isWhitelisted(String uuid) throws Exception {
		boolean b = false;
		
		QueryResult qr = HandlerStorage.getHandler("main").executeQuery("SELECT * FROM whitelist WHERE name='" + uuid + "'");
		qr.rs.last();
		
		if(qr.rs.getRow() != 0){
			b = true;
		}
		HandlerStorage.getHandler("main").closeRecources(qr.rs, qr.st);
		
		return b;
	}
	
	public static void setFriends(int from, int to) throws Exception {
		String friends = getFriends(from);
		String newFriends = friends + "," + to;
		
		HandlerStorage.getHandler("main").execute("UPDATE `users` SET `friends` = '" + newFriends + "' WHERE `id` =" + from + ";");
		
		String friends_ = getFriends(to);
		String newFriends_ = friends_ + "," + from;
		
		HandlerStorage.getHandler("main").execute("UPDATE `users` SET `friends` = '" + newFriends_ + "' WHERE `id` =" + to + ";");
	}
	
	public static boolean isFriendedWith(int id, int o) throws Exception {
		boolean b = false;
		String friends = getFriends(id);
		
		if(friends.contains(o + "")){
			b = true;
		}
		
		return b;
	}
	
}
