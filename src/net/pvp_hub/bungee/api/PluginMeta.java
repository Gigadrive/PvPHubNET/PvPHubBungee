package net.pvp_hub.bungee.api;

public class PluginMeta {

	// PvP-Hub
	public static final String prefix = "�4[�cPvP-Hub�4] �6";
	public static final String noplayer = prefix + "�cDu musst ein Spieler sein!";
	public static final String noperms = prefix + "�cKeine Rechte.";
	public static final String partyPrefix = "�9[�bParty�9] �6";
	public static final String prefixFriend = "�2[�aFreunde�2] �6";
	
	// MotDs
	public static final String mainMotd = "�c�l�oPvP-Hub.net�r �7- �6BALD!�r\n�4HungerGames �7| �a??? �7| �9???";
	public static final String motdPrefix = "�c�lPvP-Hub�7�l.�c�lnet �4�8- �4SG �7| �2INFW �7| �3DM �7| �eSPVP\n�r�f[�a1.7�f+�a1.8�f] ";
	public static final String maintenanceMotd = "�c�lPvP-Hub�7�l.�c�lnet �4�8- �4SG �7| �2INFW �7| �3DM �7| �eSPVP\n�r�4�k....�r �cWartungsarbeiten �r�4�k....";
	public static final String teamMotd = "�c�lPvP-Hub�7�l.�c�lnet �4�8- �aTeam-Server";
	
	// Others
	public static final int maxCount = 100;
	
	// Minigames
	public static final String RaceBowPrefix = "�5[�dRaceBow�5] �7";
	
	// StaffNotify
	public static final String sn_prefix = "�7[�cStaffNotify�7] �7";
	public static final String sn_prefix_chat = "�7[�cStaffNotify�7|�bTeam-Chat�7] �7";
	public static final String sn_prefix_report = "�7[�cStaffNotify�7|�4Report�7] �7";
	public static final String sn_prefix_bans = "�7[�cStaffNotify�7|�5Bans�7] �7";
	public static final String sn_prefix_join = "�7[�cStaffNotify�7|�a+] �7";
	public static final String sn_prefix_leave = "�7[�cStaffNotify�7|�c-] �7";
	
	
}
