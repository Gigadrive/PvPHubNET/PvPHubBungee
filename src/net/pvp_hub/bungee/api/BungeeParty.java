package net.pvp_hub.bungee.api;

import java.util.List;

import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.pvp_hub.bungee.PvPHubBungee;

public class BungeeParty {

	private ProxiedPlayer owner;
	private List<String> members;
	
	public BungeeParty(ProxiedPlayer owner, List <String> members){
		this.owner = owner;
		this.members = members;
		
		this.members.add(this.owner.getName());
		PvPHubBungee.getParty.put(this.owner.getName(), this.owner.getName());
	}
	
	public List<String> getMembers(){
		return this.members;
	}
	
	public ProxiedPlayer getOwner(){
		return this.owner;
	}
	
	public void sendMessage(String message){
		for(String members : this.members){
			ProxyServer.getInstance().getPlayer(members).sendMessage(new TextComponent(message));
		}
	}
	
	public void setMembers(List<String> members){
		this.members = members;
	}
	
	public void setOwner(ProxiedPlayer owner){
		this.owner = owner;
	}
	
}
