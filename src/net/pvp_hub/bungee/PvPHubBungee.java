package net.pvp_hub.bungee;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import net.md_5.bungee.BungeeCord;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Plugin;
import net.pvp_hub.bungee.api.BungeeParty;
import net.pvp_hub.bungee.cmd.AdminChat;
import net.pvp_hub.bungee.cmd.Brotkasten;
import net.pvp_hub.bungee.cmd.Find;
import net.pvp_hub.bungee.cmd.Follow;
import net.pvp_hub.bungee.cmd.Help;
import net.pvp_hub.bungee.cmd.List;
import net.pvp_hub.bungee.cmd.Lobby;
import net.pvp_hub.bungee.cmd.Maintenance;
import net.pvp_hub.bungee.cmd.Party;
import net.pvp_hub.bungee.cmd.PartyChat;
import net.pvp_hub.bungee.cmd.PrivateMessage;
import net.pvp_hub.bungee.cmd.Report;
import net.pvp_hub.bungee.cmd.SetMotD;
import net.pvp_hub.bungee.cmd.StaffList;
import net.pvp_hub.bungee.cmd.WhereAmI;
import net.pvp_hub.bungee.listener.ChatListener;
import net.pvp_hub.bungee.listener.PlayerDisconnectListener;
import net.pvp_hub.bungee.listener.PlayerJoinListener;
import net.pvp_hub.bungee.listener.ServerPingListener;
import net.pvp_hub.bungee.listener.ServerSwitchListener;
import tk.theakio.sql.HandlerStorage;
import tk.theakio.sql.MySQLData;
import tk.theakio.sql.QueryResult;
import tk.theakio.sql.SQLHandler;

public class PvPHubBungee extends Plugin {
	
	public static Map<String, BungeeParty> party = new HashMap();
	public static Map<String, String> getParty = new HashMap();
	public static Map<String, String> inviteParty = new HashMap();
	
	public static boolean isInMaintenance;
	
	private QueryResult qr = null;
	public String main = "main";
	
	public static String motd = "�b�lCLOSED BETA! �r�8- �6PvP-Hub.net/redeem";
	public static ArrayList<String> debuggers = new ArrayList<String>();
	
	public static ArrayList<ProxiedPlayer> staff = new ArrayList<ProxiedPlayer>();

	public void onEnable(){
		ProxyServer.getInstance().getPluginManager().registerCommand(this, new PrivateMessage());
		ProxyServer.getInstance().getPluginManager().registerCommand(this, new Lobby());
		ProxyServer.getInstance().getPluginManager().registerCommand(this, new List());
		ProxyServer.getInstance().getPluginManager().registerCommand(this, new Find());
		ProxyServer.getInstance().getPluginManager().registerCommand(this, new WhereAmI());
		ProxyServer.getInstance().getPluginManager().registerCommand(this, new AdminChat());
		ProxyServer.getInstance().getPluginManager().registerCommand(this, new Report());
		ProxyServer.getInstance().getPluginManager().registerCommand(this, new Party());
		ProxyServer.getInstance().getPluginManager().registerCommand(this, new PartyChat());
		ProxyServer.getInstance().getPluginManager().registerCommand(this, new Help());
		ProxyServer.getInstance().getPluginManager().registerCommand(this, new Maintenance());
		ProxyServer.getInstance().getPluginManager().registerCommand(this, new Brotkasten());
		ProxyServer.getInstance().getPluginManager().registerCommand(this, new SetMotD());
		ProxyServer.getInstance().getPluginManager().registerCommand(this, new StaffList());
		ProxyServer.getInstance().getPluginManager().registerCommand(this, new Follow());
		
		this.getProxy().getPluginManager().registerListener(this, new ServerPingListener());
		this.getProxy().getPluginManager().registerListener(this, new PlayerJoinListener());
		this.getProxy().getPluginManager().registerListener(this, new PlayerDisconnectListener());
		this.getProxy().getPluginManager().registerListener(this, new ServerSwitchListener());
		this.getProxy().getPluginManager().registerListener(this, new ChatListener());
		
		PvPHubBungee.isInMaintenance = false;
		
		debuggers.add("Zeryther");
		debuggers.add("crafter14360");
		
		SQLHandler Handler = HandlerStorage.addHandler(main, new SQLHandler());
		Handler.setMySQLOptions(MySQLData.mysql_u_host, MySQLData.mysql_u_port, MySQLData.mysql_u_user, MySQLData.mysql_u_pass, MySQLData.mysql_u_database);
		try {
			Handler.openConnection();
		} catch (Exception e) {
			System.err.println("[MySQL API von Akio Zeugs] Putt Putt!");
			e.printStackTrace();
		}
		
		for(ProxiedPlayer all : BungeeCord.getInstance().getPlayers()){
			if(all.hasPermission("staff.join")){
				PvPHubBungee.staff.add(all);
			}
		}
	}
	
	public static String getRankColor(ProxiedPlayer p){
		if(p.hasPermission("pvphub.rank.admin")){
			return "�c";
		} else if(p.hasPermission("pvphub.rank.dev")){
			return "�9";
		} else if(p.hasPermission("pvphub.rank.mod")){
			return "�e";
		} else if(p.hasPermission("pvphub.rank.team")){
			return "�a";
		} else if(p.hasPermission("pvphub.rank.build")){
			return "�3";
		} else {
			return "�7";
		}
	}
	
}
