package tk.theakio.sql;

import java.sql.PreparedStatement;
import java.sql.ResultSet;

public class QueryResult {

	public ResultSet rs;
	public PreparedStatement st;
	
	public QueryResult(PreparedStatement st, ResultSet rs) {
		this.st = st;
		this.rs = rs;
	}

}
