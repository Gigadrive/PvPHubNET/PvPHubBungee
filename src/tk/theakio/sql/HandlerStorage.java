package tk.theakio.sql;

import java.util.HashMap;

public class HandlerStorage {
	
	private static HashMap<String, SQLHandler> handlers;
	
	public static void init() {
		handlers = new HashMap<String, SQLHandler>();
	}
	
	public static HashMap<String, SQLHandler> getHandlers() {
		return handlers;
	}
	
	public static SQLHandler addHandler(String name, SQLHandler handler) {
		if(handlers == null) {
			init();
		}
		handlers.put(name, handler);
		return handler;
	}
	
	public static void removeHandler(String name) {
		if(handlers == null) {
			init();
		}
		handlers.remove(name);
	}
	
	public static SQLHandler getHandler(String name) {
		if(handlers == null) {
			init();
		}
		if(handlers.containsKey(name)) {
			return handlers.get(name);
		}
		return null;
	}

}
